﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Library.Models;

namespace Library.Controllers
{
    public class BookController : Controller
    {
        private readonly LibraryDBContext _context;

        public BookController(LibraryDBContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Search()
        {
            string key = Request.Query["keyword"].ToString().Trim();
            if (key == "")
                ViewBag.bookLst = await _context.TBook.Include(t => t.BookType).ToListAsync();
            else
            {
                var bookLst = _context.TBook.Include(t => t.BookType);
                if(Request.Query["type"].ToString()=="book")
                   ViewBag.bookLst = await (from b in bookLst where (b.BookName.Contains(key)) select b).ToListAsync();
                else if(Request.Query["type"].ToString() == "author")
                    ViewBag.bookLst = await (from b in bookLst where (b.Author.Contains(key)) select b).ToListAsync();
                else
                    ViewBag.bookLst = await (from b in bookLst where (b.Press.Contains(key)) select b).ToListAsync();
            }
            return View();
        }
        public async Task<IActionResult> Manage()
        {
            ViewBag.bookLst = await _context.TBook.Include(t => t.BookType).ToListAsync();
            ViewBag.btLst = await _context.TBookType.ToListAsync();
            return View();
        }

        public async Task<IActionResult> Create()
        {
            ViewBag.btLst = await _context.TBookType.ToListAsync();
            return View();
        }

        //方法


        /// <summary>
        /// 添加书籍
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        public async Task<Msg> AddBook(TBook book)
        {
            Msg msg = new Msg();
            try
            {
                await _context.TBook.AddAsync(book);
                await _context.SaveChangesAsync();
                msg.msg = "添加成功！";
                msg.i = 1;
                return msg;
            }
            catch (Exception)
            {
                msg.msg = "什么都没发生！";
                return msg;
            }
        }

        /// <summary>
        /// 修改书籍信息
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        public async Task<Msg> EditBook(TBook book)
        {
            Msg msg = new Msg();
            try
            {
                _context.TBook.Update(book);
                await _context.SaveChangesAsync();
                msg.msg = "修改成功！";
                msg.i = 1;
                return msg;
            }
            catch (Exception ex)
            {
                msg.msg = "什么都没发生！";
                return msg;
            }

        }


        /// <summary>
        /// 删除书籍
        /// </summary>
        /// <param name="BookID"></param>
        /// <returns></returns>
        public async Task<Msg> DeleteBook(int BookID)
        {
            Msg msg = new Msg();
            try
            {
                var book = await _context.TBook.FindAsync(BookID);
                _context.TBook.Remove(book);
                await _context.SaveChangesAsync();

                msg.msg = "删除成功！";
                msg.i = 1;
                return msg;
            }
            catch (Exception ex)
            {

                msg.msg = "什么都没发生！";
                return msg;
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Library.Models;

namespace Library.Controllers
{
    public class BorrowController : Controller
    {
        private readonly LibraryDBContext _context;

        public BorrowController(LibraryDBContext context)
        {
            _context = context;
        }

        // GET: Borrow
        public async Task<IActionResult> Index()
        {
            ViewBag.BrwLst = await _context.TBorrow
                .Include(b => b.Book)
                .Include(u => u.User)
                .ToListAsync();
            return View();
        }
    }
}

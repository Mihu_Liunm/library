﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Library.Models;

namespace Library.Controllers
{
    public class UserController : Controller
    {
        private readonly LibraryDBContext _context;

        public UserController(LibraryDBContext context)
        {
            _context = context;
        }

        // GET: User
        public async Task<IActionResult> Index()
        {
            ViewBag.user = await _context.TUser
                .Include(t => t.UserType)
                .FirstOrDefaultAsync(m => m.UserId == LoginInfo.user.UserId);
            return View();
        }

        public async Task<IActionResult> Manage()
        {
            var userLst = _context.TUser.Include(t => t.UserType);
            ViewBag.utLst = await (from s in _context.TUserType select s).ToListAsync();

            if (LoginInfo.user.UserType.UserTypeId == 1)
                ViewBag.userLst = await (from s in userLst where s.UserTypeId != 1 select s).ToListAsync();
            else
                ViewBag.userLst = await (from s in userLst where s.UserTypeId == 3 select s).ToListAsync();

            return View();
        }
        // GET: User/Create
        public async Task<IActionResult> Create()
        {
            ViewBag.utLst = await (from s in _context.TUserType select s).ToListAsync();
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<Msg> SignIn(TUser user)
        {
            Msg msg = new Msg();

            //登录判断
            if (await _context.TUser.AnyAsync(s => s.TelNumber == user.TelNumber && s.Password == user.Password))
            {
                msg.msg = "登录成功！";
                msg.i = 1;
                LoginInfo.user = await _context.TUser.Include(s=>s.UserType).SingleAsync(s => s.TelNumber == user.TelNumber && s.Password == user.Password);
                return msg;
            }
            msg.msg = "账号或密码错误！";
            return msg;
        }
        /// <summary>
        /// 退出登录
        /// </summary>
        /// <returns></returns>
        public Msg Logout()
        {
            Msg msg = new Msg();
            LoginInfo.user = new TUser();
            msg.i = 1;
            return msg;
        }

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<Msg> AddUser(TUser user)
        {
            Msg msg = new Msg();
            await _context.TUser.AddAsync(user);
            await _context.SaveChangesAsync();
            msg.msg = "添加成功！";
            msg.i = 1;
            return msg;
        }

        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public async Task<Msg> EditUser(TUser user)
        {
            Msg msg = new Msg();
            try
            {
                _context.TUser.Update(user);
                await _context.SaveChangesAsync();
                if (user.UserId==LoginInfo.user.UserId)
                    LoginInfo.user = await _context.TUser.Include(s => s.UserType).FirstOrDefaultAsync(s => s.UserId == user.UserId);

                msg.msg = "修改成功！";
                msg.i = 1;
                return msg;
            }
            catch (Exception ex)
            {
                msg.msg = "什么都没发生！";
                return msg;
            }

        }
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public async Task<Msg> DeleteUser(int UserID)
        {

            var tUser = await _context.TUser.FindAsync(UserID);
            _context.TUser.Remove(tUser);
            await _context.SaveChangesAsync();

            Msg msg = new Msg();
            msg.msg = "删除成功！";
            msg.i = 1;
            return msg;
        }
    }
}

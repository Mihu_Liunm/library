﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Library.Models
{
    public partial class LibraryDBContext : DbContext
    {
        public LibraryDBContext()
        {
        }

        public LibraryDBContext(DbContextOptions<LibraryDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TBook> TBook { get; set; }
        public virtual DbSet<TBookType> TBookType { get; set; }
        public virtual DbSet<TBorrow> TBorrow { get; set; }
        public virtual DbSet<TUser> TUser { get; set; }
        public virtual DbSet<TUserType> TUserType { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=.;Initial Catalog=LibraryDB;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<TBook>(entity =>
            {
                entity.HasKey(e => e.BookId)
                    .HasName("PK_T");

                entity.ToTable("T_Book");

                entity.Property(e => e.BookId).HasColumnName("Book_ID");

                entity.Property(e => e.Author)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.BookDate)
                    .IsRequired()
                    .HasColumnName("Book_date")
                    .HasMaxLength(20);

                entity.Property(e => e.BookName)
                    .IsRequired()
                    .HasColumnName("Book_name")
                    .HasMaxLength(50);

                entity.Property(e => e.BookRemark)
                    .HasColumnName("Book_remark")
                    .HasColumnType("text")
                    .HasDefaultValueSql("('暂无')");

                entity.Property(e => e.BookTypeId).HasColumnName("BookType_ID");

                entity.Property(e => e.ISBN)
                    .IsRequired()
                    .HasColumnName("ISBN")
                    .HasMaxLength(20);

                entity.Property(e => e.Press)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Price).HasColumnType("money");

                entity.HasOne(d => d.BookType)
                    .WithMany(p => p.TBook)
                    .HasForeignKey(d => d.BookTypeId)
                    .HasConstraintName("FK_T_Book_T_BookType");
            });

            modelBuilder.Entity<TBookType>(entity =>
            {
                entity.HasKey(e => e.BookTypeId);

                entity.ToTable("T_BookType");

                entity.Property(e => e.BookTypeId).HasColumnName("BookType_ID");

                entity.Property(e => e.BookTypeName)
                    .IsRequired()
                    .HasColumnName("BookType_name")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<TBorrow>(entity =>
            {
                entity.HasKey(e => e.BorrowId);

                entity.ToTable("T_Borrow");

                entity.Property(e => e.BorrowId).HasColumnName("Borrow_ID");

                entity.Property(e => e.BookId).HasColumnName("Book_ID");

                entity.Property(e => e.BoorrowState)
                    .IsRequired()
                    .HasColumnName("Boorrow_state")
                    .HasMaxLength(10)
                    .HasDefaultValueSql("(N'no')");

                entity.Property(e => e.BorrowRemark)
                    .HasColumnName("Borrow_remark")
                    .HasColumnType("text");

                entity.Property(e => e.Cycle)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.LendDate)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasDefaultValueSql("(CONVERT([varchar](100),getdate(),(102)))");

                entity.Property(e => e.ReturnDate).HasMaxLength(20);

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.HasOne(d => d.Book)
                    .WithMany(p => p.TBorrow)
                    .HasForeignKey(d => d.BookId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Borrow_T_Book");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TBorrow)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_Borrow_T_User");
            });

            modelBuilder.Entity<TUser>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("T_User");

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.Property(e => e.Identity)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Sex)
                    .IsRequired()
                    .HasMaxLength(2);

                entity.Property(e => e.TelNumber)
                    .IsRequired()
                    .HasMaxLength(11);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnName("User_name")
                    .HasMaxLength(10);

                entity.Property(e => e.UserRemark)
                    .HasColumnName("User_remark")
                    .HasColumnType("text");

                entity.Property(e => e.UserTypeId).HasColumnName("UserType_ID");

                entity.HasOne(d => d.UserType)
                    .WithMany(p => p.TUser)
                    .HasForeignKey(d => d.UserTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_T_User_T_User");
            });

            modelBuilder.Entity<TUserType>(entity =>
            {
                entity.HasKey(e => e.UserTypeId);

                entity.ToTable("T_UserType");

                entity.Property(e => e.UserTypeId).HasColumnName("UserType_ID");

                entity.Property(e => e.UserTypeName)
                    .IsRequired()
                    .HasColumnName("UserType_name")
                    .HasMaxLength(10);
            });
        }
    }
}

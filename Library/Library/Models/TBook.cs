﻿using System;
using System.Collections.Generic;

namespace Library.Models
{
    public partial class TBook
    {
        public TBook()
        {
            TBorrow = new HashSet<TBorrow>();
        }

        public int BookId { get; set; }
        public int? BookTypeId { get; set; }
        public string ISBN { get; set; }
        public string BookName { get; set; }
        public string Author { get; set; }
        public string Press { get; set; }
        public string BookDate { get; set; }
        public decimal Price { get; set; }
        public string BookRemark { get; set; }

        public virtual TBookType BookType { get; set; }
        public virtual ICollection<TBorrow> TBorrow { get; set; }
    }
}

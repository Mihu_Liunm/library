﻿using System;
using System.Collections.Generic;

namespace Library.Models
{
    public partial class TBookType
    {
        public TBookType()
        {
            TBook = new HashSet<TBook>();
        }

        public int BookTypeId { get; set; }
        public string BookTypeName { get; set; }

        public virtual ICollection<TBook> TBook { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Library.Models
{
    public partial class TBorrow
    {
        public int BorrowId { get; set; }
        public int UserId { get; set; }
        public int BookId { get; set; }
        public string LendDate { get; set; }
        public string ReturnDate { get; set; }
        public string Cycle { get; set; }
        public string BoorrowState { get; set; }
        public string BorrowRemark { get; set; }

        public virtual TBook Book { get; set; }
        public virtual TUser User { get; set; }
    }
}

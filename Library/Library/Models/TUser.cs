﻿using System;
using System.Collections.Generic;

namespace Library.Models
{
    public partial class TUser
    {
        public TUser()
        {
            TBorrow = new HashSet<TBorrow>();
        }

        public int UserId { get; set; }
        public int UserTypeId { get; set; }
        public string TelNumber { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string Identity { get; set; }
        public string Sex { get; set; }
        public string UserRemark { get; set; }

        public virtual TUserType UserType { get; set; }
        public virtual ICollection<TBorrow> TBorrow { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Library.Models
{
    public partial class TUserType
    {
        public TUserType()
        {
            TUser = new HashSet<TUser>();
        }

        public int UserTypeId { get; set; }
        public string UserTypeName { get; set; }

        public virtual ICollection<TUser> TUser { get; set; }
    }
}

#pragma checksum "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "426797dd115975fcc760ce710d768b56faa6cf4c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_User_Index), @"mvc.1.0.view", @"/Views/User/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/User/Index.cshtml", typeof(AspNetCore.Views_User_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Administrator\Desktop\Library\Library\Views\_ViewImports.cshtml"
using Library;

#line default
#line hidden
#line 2 "C:\Users\Administrator\Desktop\Library\Library\Views\_ViewImports.cshtml"
using Library.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"426797dd115975fcc760ce710d768b56faa6cf4c", @"/Views/User/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dadb7a731bfbb305c411bc5eb7a307dbd6008a89", @"/Views/_ViewImports.cshtml")]
    public class Views_User_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
  
    ViewData["Title"] = "个人中心";
    Layout = "~/Views/Shared/_Layout.cshtml";
    var user = ViewBag.user;

#line default
#line hidden
            BeginContext(117, 286, true);
            WriteLiteral(@"
<script>
    $(""a[href='/User/Index']"").parent('li').addClass('active')
</script>
<script>
        function EditUser() {
        $.ajax({
            url: ""/User/EditUser"",
            type: ""post"",
            dataType: ""json"",
            data: {
                UserID: '");
            EndContext();
            BeginContext(404, 11, false);
#line 17 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
                    Write(user.UserId);

#line default
#line hidden
            EndContext();
            BeginContext(415, 251, true);
            WriteLiteral("\',\r\n                TelNumber:$(\"#teln\").val(),\r\n                Password:$(\"#passw\").val(),\r\n                UserName:$(\"#usrnme\").val(),\r\n                Identity:$(\"#idty\").val(),\r\n                Sex:$(\"#sex\").val(),\r\n                UserTypeId: \'");
            EndContext();
            BeginContext(667, 24, false);
#line 23 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
                        Write(user.UserType.UserTypeId);

#line default
#line hidden
            EndContext();
            BeginContext(691, 608, true);
            WriteLiteral(@"',
                UserRemark: $(""#uermrk"").val()
            },
            success: function (data) {
                alert(data.msg)
                if (data.i == 1) {
                    location.reload();
                }
            },
            error: function (xhl) {
                alert(""发生未知错误"")
                console.log(xhl.responseText)
            }
        })
    }
</script>
<h1>个人中心</h1>

<hr />
<div class=""row"">
    <div class=""col-md-4"">
        <div class=""form-group"">
            <h4>姓名</h4>
            <input id=""usrnme"" class=""form-control"" type=""text""");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 1299, "\"", 1321, 1);
#line 46 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
WriteAttributeValue("", 1307, user.UserName, 1307, 14, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1322, 190, true);
            WriteLiteral(" />\r\n            <span class=\"text-danger\"></span>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <h4>电话号码</h4>\r\n            <input id=\"teln\" class=\"form-control\" type=\"text\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 1512, "\"", 1535, 1);
#line 51 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
WriteAttributeValue("", 1520, user.TelNumber, 1520, 15, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1536, 189, true);
            WriteLiteral(" />\r\n            <span class=\"text-danger\"></span>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <h4>密码</h4>\r\n            <input id=\"passw\" class=\"form-control\" type=\"text\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 1725, "\"", 1747, 1);
#line 56 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
WriteAttributeValue("", 1733, user.Password, 1733, 14, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1748, 190, true);
            WriteLiteral(" />\r\n            <span class=\"text-danger\"></span>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <h4>身份证号</h4>\r\n            <input id=\"idty\" class=\"form-control\" type=\"text\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 1938, "\"", 1960, 1);
#line 61 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
WriteAttributeValue("", 1946, user.Identity, 1946, 14, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1961, 179, true);
            WriteLiteral(" />\r\n            <span class=\"text-danger\"></span>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <h4>性别</h4>\r\n            <select id=\"sex\" class=\"form-control\">\r\n");
            EndContext();
#line 67 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
                  
                    string[] sexArr = { "不详", "男", "女" };
                    foreach (string item in sexArr)
                    {
                        

#line default
#line hidden
#line 71 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
                         if (item == user.Sex)
                        {

#line default
#line hidden
            BeginContext(2370, 28, true);
            WriteLiteral("                            ");
            EndContext();
            BeginContext(2398, 45, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "426797dd115975fcc760ce710d768b56faa6cf4c8237", async() => {
                BeginContext(2430, 4, false);
#line 73 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
                                                      Write(item);

#line default
#line hidden
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            BeginWriteTagHelperAttribute();
#line 73 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
                               WriteLiteral(item);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __tagHelperExecutionContext.AddHtmlAttribute("selected", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2443, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 74 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
                            continue;
                        }

#line default
#line hidden
            BeginContext(2511, 24, true);
            WriteLiteral("                        ");
            EndContext();
            BeginContext(2535, 36, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "426797dd115975fcc760ce710d768b56faa6cf4c10778", async() => {
                BeginContext(2558, 4, false);
#line 76 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
                                         Write(item);

#line default
#line hidden
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            BeginWriteTagHelperAttribute();
#line 76 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
                           WriteLiteral(item);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2571, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 77 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
                    }
                

#line default
#line hidden
            BeginContext(2615, 216, true);
            WriteLiteral("            </select>\r\n            <span class=\"text-danger\"></span>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <h4>备注</h4>\r\n            <textarea id=\"uermrk\" class=\"form-control\" type=\"textarea\">");
            EndContext();
            BeginContext(2832, 15, false);
#line 84 "C:\Users\Administrator\Desktop\Library\Library\Views\User\Index.cshtml"
                                                                  Write(user.UserRemark);

#line default
#line hidden
            EndContext();
            BeginContext(2847, 232, true);
            WriteLiteral("</textarea>\r\n            <span class=\"text-danger\"></span>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <div onclick=\"EditUser()\" class=\"btn btn-primary btn-block\">Save</div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
